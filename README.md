Let me start by saying that this architecture is overkill for the requirements of this project.  This was just the quickest way for me to get started based on some recent spring experience so that's the way I went, and it more reflects what my line of thinking would be for a real project. That said, I always feel on the fence as to whether ORMs and heavy frameworks really are worth the investment.

This is a Spring project with Spring Data JPA (+ Hibernate).  I didn't pull in spring security because it was easier to write one-off code for the delete method. It uses the H2 database in-memory, and loads data from the resource file "data.json".

The HTTP stuff is served at /employees and follows pretty standard JSON HTTP API conventions but doesn't use HATEOAS or any linked data formats.  For security, the app generates and prints a UUID to the console, which is the shared secret API key for this session.  Calls to delete an employee must pass that as the Authorization header, but the rest are unauthenticated.

Method | Path | Action
-------|------|------
GET|/employees|Retrieve all active employees
GET|/employees/all|Peek at all employees so you can see the soft-delete work
GET|/employees/{id}|Retrieve employee {id}
POST|/employees|Create employee with request body
PATCH|/employees/{id}|Given some subset of fields from the POST method, update relevant fields for employee {id}
DELETE|/employees/{id}|Soft-delete employee.  Requires authorization

As far as proper code, There's an entity, controller, service, and repository class, and some exception types.  Honestly, Spring generates so much that I didn't really have much opportunity to use Java 8 features like streams, but most of that will be found in the EmployeeService class, which makes use of map and such, and there's plenty of Optionals to be found.

There's a few unit tests too, for stuff that I think is well-suited for it. System tests were manual.

Some notes/weirdness:
* I used a system generated sequential ID for the primary key. There are plenty of debatable choices for this but I just chose this one because it's familiar.
* There's no constraints like not null since the description didn't require any.
* when you post/patch an employee, the dates in the response spit back the time too, while when you fetch data, you only get back the date.  The latter is what I wanted the whole time but something weird's happening there.
* The PATCH update method doesn't support updating fields to null, as it doesn't know the difference of a null value explicitly being passed and the key-value pair not being provided.
package com.example.employeeapi;

import org.junit.Before;
import org.junit.Test;

import org.junit.Assert;

import java.util.Date;

public class EmployeeServiceTests {
    Employee employee;

    Long originalId;
    Employee.EmployeeStatus originalStatus;
    String originalFirstName;
    Character originalMiddleInitial;
    String originalLastName;
    Date originalDateOfBirth;
    Date originalDateOfEmployment;

    Employee updatedEmployee;

    Long updatedId;
    Employee.EmployeeStatus updatedStatus;
    String updatedFirstName;
    Character updatedMiddleInitial;
    String updatedLastName;
    Date updatedDateOfBirth;
    Date updatedDateOfEmployment;

    @Before
    public void before() {
        originalId = 1L;
        originalStatus = Employee.EmployeeStatus.ACTIVE;
        originalFirstName = "originalFirstName";
        originalMiddleInitial = 'A';
        originalLastName = "originalLastName";
        originalDateOfBirth = new Date(55555);
        originalDateOfEmployment = new Date(66666);

        employee = new Employee();
        employee.setID(originalId);
        employee.setStatus(originalStatus);
        employee.setFirstName(originalFirstName);
        employee.setMiddleInitial(originalMiddleInitial);
        employee.setLastName(originalLastName);
        employee.setDateOfBirth(originalDateOfBirth);
        employee.setDateOfEmployment(originalDateOfEmployment);

        updatedId = 2L;
        updatedStatus = Employee.EmployeeStatus.INACTIVE;
        updatedFirstName = "updatedFirstName";
        updatedMiddleInitial = 'B';
        updatedLastName = "updatedLastName";
        updatedDateOfBirth = new Date(77777);
        updatedDateOfEmployment = new Date(88888);

        updatedEmployee = new Employee();
        updatedEmployee.setID(updatedId);
        updatedEmployee.setStatus(updatedStatus);
        updatedEmployee.setFirstName(updatedFirstName);
        updatedEmployee.setMiddleInitial(updatedMiddleInitial);
        updatedEmployee.setLastName(updatedLastName);
        updatedEmployee.setDateOfBirth(updatedDateOfBirth);
        updatedEmployee.setDateOfEmployment(updatedDateOfEmployment);
    }

    @Test
    public void updateEmployeeWithNullUpdateStaysSame() {

        EmployeeService.updateEmployee(employee, null);

        Assert.assertEquals(originalId, employee.getID());
        Assert.assertEquals(originalStatus, employee.getStatus());
        Assert.assertEquals(originalFirstName, employee.getFirstName());
        Assert.assertEquals(originalMiddleInitial, employee.getMiddleInitial());
        Assert.assertEquals(originalLastName, employee.getLastName());
        Assert.assertEquals(originalDateOfBirth, employee.getDateOfBirth());
        Assert.assertEquals(originalDateOfEmployment, employee.getDateOfEmployment());
    }

    @Test
    public void updateEmployeeUpdatesSpecificFields() {

        EmployeeService.updateEmployee(employee, updatedEmployee);

        Assert.assertEquals(originalId, employee.getID());
        Assert.assertEquals(originalStatus, employee.getStatus());
        Assert.assertEquals(updatedFirstName, employee.getFirstName());
        Assert.assertEquals(updatedMiddleInitial, employee.getMiddleInitial());
        Assert.assertEquals(updatedLastName, employee.getLastName());
        Assert.assertEquals(updatedDateOfBirth, employee.getDateOfBirth());
        Assert.assertEquals(updatedDateOfEmployment, employee.getDateOfEmployment());
    }


    @Test
    public void deactivateMarksInactive() {
        employee.setStatus(Employee.EmployeeStatus.ACTIVE);

        EmployeeService.deactivate(employee);

        Assert.assertEquals(Employee.EmployeeStatus.INACTIVE, employee.getStatus());
    }
}

package com.example.employeeapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * This is where anything extra not handled by the repository alone is done
 */
@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Soft delete by setting to inactive
     * @param id
     * @return
     * @throws SoftDeleteNotFoundException if not found
     */
    public Employee softDeleteById(Long id) {
        // A few java 8 features.
        Employee employee = employeeRepository.findByIdWhereActive(id) // Returns an optional
                .map(EmployeeService::deactivate) // If found, set it inactive
                .orElseThrow(SoftDeleteNotFoundException::new); // If not, throw an exception
        employeeRepository.save(employee);
        return employee;
    }

    /**
     * Update by ID with the attributes of an employee object with patches
     * Constraint: the way this set up doesn't allow you to update a field to null.
     * @param id the employee ID
     * @param updates the Employee object with non-null fields as updates to be made
     * @return the mutated object
     */
    public Employee updateById(Long id, Employee updates) {
        Employee employee = employeeRepository.findByIdWhereActive(id)
                .orElseThrow(SoftDeleteNotFoundException::new);
        updateEmployee(employee, updates);
        employeeRepository.save(employee);
        return employee;
    }

    /**
     * A good target for unit tests, as while it's not pure, it's isolated
     * Could go in a separate module, but seems like overkill
     */
    public static Employee updateEmployee(Employee employee, Employee updates) {
        if (updates != null) {
            // Not exciting, but it's only a few fields, so manually support some updates.
            // Don't allow ID or status to be updated
            if (updates.getFirstName() != null) {
                employee.setFirstName(updates.getFirstName());
            }
            if (updates.getMiddleInitial() != null) {
                employee.setMiddleInitial(updates.getMiddleInitial());
            }
            if (updates.getLastName() != null) {
                employee.setLastName(updates.getLastName());
            }
            if (updates.getDateOfBirth() != null) {
                employee.setDateOfBirth(updates.getDateOfBirth());
            }
            if (updates.getDateOfEmployment() != null) {
                employee.setDateOfEmployment(updates.getDateOfEmployment());
            }
        }
        return employee;
    }

    /**
     * Mutating doesn't return anything; this as separate method makes the map function happy
     * when using Java 8 streams
     * @param employee the employee
     * @return the same employee, mutated
     */
    public static Employee deactivate(Employee employee) {
        employee.setStatus(Employee.EmployeeStatus.INACTIVE);
        return employee;
    }
}

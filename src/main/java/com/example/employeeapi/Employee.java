package com.example.employeeapi;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.TemporalType.DATE;

/**
 * JPA entity for Employee
 * Lombok is used to generate all the boilerplate
 */
@Entity
@lombok.Getter
@lombok.Setter
@lombok.RequiredArgsConstructor
@lombok.ToString
@lombok.EqualsAndHashCode(exclude = { "ID" })
public class Employee {

    /**
     * The employee ID
     * Choosing an auto generated ID since that is a common case
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long ID;

    private String FirstName;

    /**
     * Hibernate is being annoying and making this a varchar(255)
     */
    private Character MiddleInitial;

    private String LastName;

    @Temporal(DATE)
    private Date DateOfBirth;

    @Temporal(DATE)
    private Date DateOfEmployment;

    @Enumerated(EnumType.STRING)
    private EmployeeStatus Status;

    public enum EmployeeStatus { ACTIVE, INACTIVE };
}

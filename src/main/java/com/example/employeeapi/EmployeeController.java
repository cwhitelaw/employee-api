package com.example.employeeapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This is a thin controller layer delegating most handling to the repo and service
 */
@RestController
@RequestMapping("/employees")
public class EmployeeController {
    @Autowired // Autowired seems to be a bit controversial but I prefer it so far
    EmployeeRepository employeeRepository;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    CustomSecurityContext securityContext;

    @GetMapping
    public Iterable<Employee> getAll() {
        return employeeRepository.findAllWhereActive();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Employee> get(@PathVariable Long id) {
        return employeeRepository.findByIdWhereActive(id)
                .map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * Normally you'd have a database that you could look at to get under the hood.
     * In this case, the database is in memory, so this allows us to bypass the normal filter
     * @return all employees, active and inactive
     */
    @GetMapping("/all")
    public Iterable<Employee> getAllIncludingInactive() {
        return employeeRepository.findAll();
    }

    @PostMapping
    public Employee create(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @PatchMapping("/{id}")
    public Employee update(@PathVariable Long id, @RequestBody Employee updates) {
        return employeeService.updateById(id, updates);
    }

    /**
     * Soft deletes an employee
     * @param id the employee id
     */
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id, @RequestHeader("Authorization") String authorizationHeader) throws SoftDeleteNotFoundException {
        if (authorizationHeader == null || !authorizationHeader.equals(securityContext.getSharedSecret())) {
            throw new UnauthorizedException();
        }
        employeeService.softDeleteById(id);
    }
}

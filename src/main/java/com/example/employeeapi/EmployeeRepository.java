package com.example.employeeapi;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * This defines the employee JPA repository and supports CRUD operations.
 * However we also want to filter active employees most of the time, so we add
 * additional queries for that here.
 */
@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    @Query("SELECT e from Employee e WHERE Status = 'ACTIVE'")
    Iterable<Employee> findAllWhereActive();

    @Query("SELECT e from Employee e WHERE Status = 'ACTIVE' AND e.id = ?1")
    Optional<Employee> findByIdWhereActive(Long id);

}

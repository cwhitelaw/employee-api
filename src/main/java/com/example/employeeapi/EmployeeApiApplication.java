package com.example.employeeapi;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;
import org.springframework.util.Base64Utils;

import java.util.UUID;

@SpringBootApplication
public class EmployeeApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeApiApplication.class, args);
	}

    /**
     * This populates the repository with the deserialized contents of data.json
     */
	@Bean
    public Jackson2RepositoryPopulatorFactoryBean getRespositoryPopulator() {
        Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();
        factory.setResources(new Resource[] {new ClassPathResource("data.json")});
        return factory;
    }

    @Bean
    CustomSecurityContext getSecurityContext() {
	    String secret = UUID.randomUUID().toString();
        System.out.println("The secret this time is " + secret);
        CustomSecurityContext context = new CustomSecurityContext(secret);
	    return context;
    }
}

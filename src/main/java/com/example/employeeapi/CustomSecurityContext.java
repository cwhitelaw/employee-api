package com.example.employeeapi;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * A simple wrapper class for easy injection
 */
@Data
@RequiredArgsConstructor
public class CustomSecurityContext {
    /**
     * The shared secret for authorized HTTP calls
     */
    private final String sharedSecret;
}

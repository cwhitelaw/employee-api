package com.example.employeeapi;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This exception is thrown when we want to abort the operation.
 * The annotation below tells spring that when it's caught, the handler
 * should respond with a 404 response status.
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class SoftDeleteNotFoundException extends RuntimeException {
}
